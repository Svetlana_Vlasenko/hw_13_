<?php
class Administrator extends Person{
    public $working_day = "Working day";

    public function __construct($full_name,$phone,$email,$role,$working_day){
        parent::__construct($full_name,$phone,$email,$role);
        $this->working_day = $working_day;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . ' ' .$this->working_day;
    }
}



?>