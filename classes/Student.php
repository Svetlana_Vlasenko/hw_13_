<?php
class Student extends Person{
    public $averange_mark = "Averange mark";
    
    public function __construct($full_name,$phone,$email,$role,$averange_mark){
        parent::__construct($full_name,$phone,$email,$role);
        $this->averange_mark = $averange_mark;
    }
    
    public function getVisitCard(){
        return parent::getVisitCard() . ' ' .$this->averange_mark;
    }
}

?>