<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Person.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Teacher.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Administrator.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Student.php';
// запрос на получение данных из таблицы
try{
  $sql="SELECT id,full_name,phone,email,role,averange_mark,subject,working_day
  FROM members;";
  $responseStatmentObject = $db->query($sql);
  $members = $responseStatmentObject ->fetchAll();
}catch(Exception $e){
  die('Problem with getting data<br>'.$e->getMessage());
}
// создание ООП
$objects = [];
foreach($members as $member){
  if($member['role'] === 'student'){
    $objects[] = new Student($member['full_name'],
      $member['phone'],
      $member['email'],
      $member['role'],
      $member['averange_mark']
    );
  }elseif($member['role'] === 'teacher'){
    $objects[] = new Teacher($member['full_name'],
      $member['phone'],
      $member['email'],
      $member['role'],
      $member['subject']
    );
  }else($member['role'] === 'administrator'){
    $objects[] = new Administrator($member['full_name'],
      $member['phone'],
      $member['email'],
      $member['role'],
      $member['working_day']
    )
  };
};
?>