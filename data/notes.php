<?php
$notes =[
  [
    'full_name' =>'John Doe',
    'phone' =>'123-213-12',
    'email' =>'john@gmail.com',
    'role' =>'student',
    'averange_mark' =>8.7
  ],
  [
    'full_name' =>'Bob Roy',
    'phone' =>'123-456-78',
    'email' =>'bob@gmail.com',
    'role' =>'student',
    'averange_mark' =>9.2
  ],
  [
    'full_name' =>'Mark Moore',
    'phone' =>'321-654-87',
    'email' =>'mark@gmail.com',
    'role' =>'student',
    'averange_mark' =>5.5
  ],
  [
    'full_name' =>'Indiana Jones',
    'phone' =>'231-040-45',
    'email' =>'indiana@gmail.com',
    'role' =>'student',
    'averange_mark' =>8.9
  ],
  [
    'full_name' =>'Robert Anderson',
    'phone' =>'321-557-66',
    'email' =>'robert@gmail.com',
    'role' =>'student',
    'averange_mark' =>7.8
  ],
  [
    'full_name' =>'Ava Wilson',
    'phone' =>'457-450-90',
    'email' =>'ava@gmail.com',
    'role' =>'student',
    'averange_mark' =>9.9
  ],
  [
    'full_name' =>'Isabella Adrian',
    'phone' =>'546-478-21',
    'email' =>'isabella@gmail.com',
    'role' =>'teacher',
    'subject' =>"PHP basics"
  ],
  [
    'full_name' =>'Jack Austin',
    'phone' =>'547-647-85',
    'email' =>'jack@gmail.com',
    'role' =>'teacher',
    'subject' =>'PHP advanced'
  ],
  [
    'full_name' =>'Isla Kirk',
    'phone' =>'874-475-31',
    'email' =>'isla@gmail.com',
    'role' =>'administrator',
    'working_day' =>'Monday'
  ],
  [
    'full_name' =>'Thomas Backer',
    'phone' =>'647-757-65',
    'email' =>'thomasgmail.com',
    'role' =>'administrator',
    'working_day' =>'Wednesday'
  ],
  
];
//foreach($notes as $key =>$note ){
//  echo '<br>';
//  print_r( $note);
//}