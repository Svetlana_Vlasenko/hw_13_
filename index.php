<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/index.php';
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>

<div class="content">
  <div class="row d-flex justify-content-center">
    <div class="col-6 ">
      <h1>All university data</h1>
        <ul class="list-group">
        <!-- вывод всех данных методом getVisitCard -->
          <?php foreach($objects as $value):?>
            <li class="list-group-item">
              <?=$value->getVisitCard()?>
            </li>
          <?php endforeach;?>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>