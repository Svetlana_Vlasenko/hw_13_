<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
//запрос на получение данных из таблицы
try{
  $sql="SELECT id,full_name,phone,email,role,averange_mark,subject,working_day
  FROM members;";
  $responseStatmentObject = $db->query($sql);
  $members = $responseStatmentObject ->fetchAll();
}catch(Exception $e){
    die('Problem with getting data<br>'.$e->getMessage());
} 
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>
  
<div class="container">
    <div class="row">
    <!-- вывод всех записей -->
        <?php foreach($members as  $member):?>
            <div class=" col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?=$member['full_name']?></h5>
                        <p class="card-title"><?=$member['role']?></p>
                        <a href="/university/two.php?id=<?=$member['id']?>">More details</a>  
                    </div>
                </div>    
            </div>
        <?php endforeach;?>
    </div>
</div>     

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>